var database = firebase.database();
var userLogged;
var USER_ID = window.location.search.match(/\?userId=(.*)/)[1];

$(document).ready(function () {
  //verifica se usuário está logado
  firebase.auth().onAuthStateChanged(function (user) {
    if (!user) {
      window.location = "login.html"
    } else {
      userLogged = user;
      $("#helloUser").text(userLogged.displayName);
    }
  });

  $(".image").append("<img src='http://placekitten.com/g/200/300'>");

  database.ref("users").once("value").then(function (snapshot) {
    snapshot.forEach(function (childSnapshot) {
      var childKey = childSnapshot.key;
      var childData = childSnapshot.val();
      createUsers(childData.name, childKey);
    })
  });

  $("#logout-button").click(function (event) {
    event.preventDefault();

    firebase.auth().signOut()
      .then(function () {
        window.location = "login.html"
      })
      .catch(function (error) {});
  });

  $(".send-post").click(function (event) {
    event.preventDefault();

    var text = $("#post-input").val();
    $("#post-input").val("");
    var visible = $("#post-visible").val();

    var newPostInDB = database.ref('users/' + USER_ID + '/posts/').push({
      text: text,
      visible: visible,
      author: userLogged.displayName,
      authorId: userLogged.uid
    });

    createMyPost(text, newPostInDB.key, userLogged.displayName);
  });

  listAllPosts(USER_ID);
  $("#filter-posts").change(function () {
    $(".posts-list").empty();
    if (this.value === 'my-posts') {
      listMyPosts(USER_ID)
    } else {
      listAllPosts(USER_ID);
    }
  });
});

function createMyPost(text, key, userName) {
  $(".posts-list").prepend(`
          <li>
            <span>${userName}</span>
            <span data-text-id="${key}">${text}</span>
            <button data-edit-id="${key}">Editar</button>
            <button data-delete-id="${key}">Excluir</button>
          </li>
        `);

  $(`button[data-delete-id=${key}]`).click(function () {
    $(this).parent().remove();
    database.ref('users/' + '/posts/' + USER_ID + key).remove();
  });

  $(`button[data-edit-id=${key}]`).click(function () {
    var newText = prompt(`Alterar texto: ${text}`);
    $(`span[data-text-id=${key}]`).html(newText);
    database.ref('users/' + '/posts/' + USER_ID + key).update({
      text: newText
    })
  });
}

function listMyPosts(userId) {
  database.ref('users/' + userId + '/posts').once('value').then(function (snapshot) {
    snapshot.forEach(function (childSnapshot) {
      var childKey = childSnapshot.key;
      var childData = childSnapshot.val();
      createMyPost(childData.text, childKey, childData.author);
    });
  });
}

function listAllPosts(userId) {
  database.ref('users').once('value').then(function (usersSnapshot) {
    usersSnapshot.forEach(function (usersChildSnapshot) {
      database.ref('users/' + usersChildSnapshot.key + '/posts').once('value').then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
          var childKey = childSnapshot.key;
          var childData = childSnapshot.val();
          if (usersChildSnapshot.key === userId) {
            createMyPost(childData.text, childKey, childData.author);
          } else {
            createOtherPost(childData.text, childKey, childData.author);
          }
        });
      });
    });
  });
}

function createOtherPost(text, key, userName) {
  $(".posts-list").prepend(`
      <li>
        <span>${userName}</span>
        <span data-text-id="${key}">${text}</span>
      </li>
  `);
}

function createUsers(userName, key) {
  if (key !== USER_ID) {
    $(".users-list").append(`
    <li>
      <span>${userName}</span>
      <button data-user-id="${key}">seguir</button>
    </li>
    `);
  }

  $(`button[data-user-id=${key}]`).click(function () {
    database.ref('users/' + USER_ID + '/friendship').push({
      friendId: key
    });
  });

  $(`button[data-user-id=${key}]`).click(function() {
    if($(this).text() === "seguir") {
      database.ref('users/' + USER_ID + '/friendship').push({
        friendId: key
      });
      $(this).text("seguindo").off("click");
    }
  });
}

function openNav() {
  document.getElementById("mySidenav").style.width = "20%";
  document.getElementById("content").style.width = "80%";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("content").style.width = "100%";
}
